help:
	@echo "Available commands:"
	@echo "  - build (builds docker images)"
	@echo "  - up (starts all the services in background)"
	@echo "  - check-containers (checks containers work)"
	@echo "  - check-locales (checks locales)"
	@echo "  - check-db-exists (checks db exists)"
	@echo "  - check-db-table (checks db tables)"
	@echo "  - migrate (runs all migrations)"
	@echo "  - show-migrations (shows all migrations)"
	@echo "  - rollback-migrations app_name=app_name migration_name=migration_name (rollbacks migrations)"
	@echo "  - create-user username=username first_name=first_name last_name=last_name email=email password=password (creates user)"
	@echo "  - collect-static (collects static files)"
	@echo "  - test (runs tests)"
	@echo "  - down (stops all containers)"

# Builds docker images
build:
	docker compose build --no-cache

# Starts all the services in background
up:
	docker compose up -d

# Checks containers work
check-containers:
	docker compose ps

# Checks locales
check-locales:
	docker compose exec postgresql locale -a

# Checks db exists
check-db-exists:
	docker compose exec postgresql psql -U postgres -l

# Checks db tables
check-db-tables:
	docker compose exec postgresql psql -U postgres -d mrp -c '\dt'

# Runs all migrations
migrate:
	docker compose run --rm app ./manage.py migrate

# Shows all migrations
show-migrations:
	docker compose run --rm app ./manage.py showmigrations

# Rollbacks migrations
# `make rollback-migrations app_name=app_name migration_name=migration_name`
rollback-migrations:
	docker compose run --rm app ./manage.py migrate ${app_name} ${migration_name}

# Collects static files
collect-static:
	docker compose run --rm app ./manage.py collectstatic

# Creates user
create-user:
	docker compose run --rm app ./manage.py create_user --username ${username} --first_name ${first_name} --last_name ${last_name} --email ${email} --password ${password}

# Runs tests
test:
	docker compose run --rm app ./manage.py test

# Stops all containers
down:
	docker compose down

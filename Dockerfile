FROM python:3.11.6-alpine

ENV HOME /var/www/backend
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR $HOME

RUN apk update && apk add --no-cache nodejs npm

RUN mkdir $HOME/staticfiles $HOME/node_modules

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./package.json .
RUN npm i

COPY . .

import json

from django.core.management import BaseCommand
from django.db import transaction

from app.users.api.v1.serializers import UserSerializer
from app.users.api.v1.utils import JWTAuthUtil
from app.users.models import User


class Command(BaseCommand):
    serializer_class = UserSerializer
    auth_util_class = JWTAuthUtil

    def add_arguments(self, parser):
        parser.add_argument('--username', type=str)
        parser.add_argument('--first_name', type=str)
        parser.add_argument('--last_name', type=str)
        parser.add_argument('--email', type=str)
        parser.add_argument('--password', type=str)

    def handle(self, *args, **options):
        serializer = self.serializer_class(data=options)
        if serializer.is_valid():
            with transaction.atomic():
                user = User.objects.create_user(**serializer.validated_data)
                auth_util = self.auth_util_class()
                tokens = auth_util.refresh_tokens(user)
                return self.stdout.write(
                    self.style.SUCCESS(
                        '\nUser created successfully. '
                        '\n\nRefresh token: {refresh_token}'
                        '\n\nAccess token: {access_token}'.format(
                            refresh_token=tokens['refresh'],
                            access_token=tokens['access']
                        )
                    )
                )
        return self.stderr.write('\n{}'.format(json.dumps(serializer.errors)))

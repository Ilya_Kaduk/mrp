import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Sum


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    balance = models.DecimalField(max_digits=16, decimal_places=8, default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def update_balance(self):
        balance = \
            self.coin_transactions.aggregate(Sum('amount')).get('amount__sum')
        if balance is None:
            balance = 0
        self.balance = balance
        self.save()

from rest_framework_simplejwt.tokens import RefreshToken

from app.users.models import User


class JWTAuthUtil:
    def refresh_tokens(self, user: User) -> dict:
        tokens = RefreshToken.for_user(user)
        return {
            'refresh': str(tokens),
            'access': str(tokens.access_token)
        }

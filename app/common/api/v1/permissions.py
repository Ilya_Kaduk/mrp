from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    message = 'Access denied. You do not own this entity.'

    def has_object_permission(self, request, view, obj):
        return str(obj.owner_id) == str(request.user.id)

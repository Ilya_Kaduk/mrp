from django.contrib.auth.hashers import make_password

from app.coins.models import CoinTransaction
from app.users.models import User


class ApiTestUtil:
    USERS = [
        User(
            username='john_doe',
            email='john_doe@gmail.com',
            password=make_password('john_doe@1234'),
            is_superuser=False
        ),
        User(
            username='jane_doe',
            email='jane_doe@gmail.com',
            password=make_password('jane_doe@1234'),
            is_superuser=False
        )
    ]

    def create_test_users(self):
        User.objects.bulk_create(self.USERS)

    def claim_coins(self, user):
        user_balance = user.balance
        balance_after = user_balance + CoinTransaction.TYPE_CLAIM_COIN_AMOUNT
        CoinTransaction.objects.create(
            owner=user,
            amount=CoinTransaction.TYPE_CLAIM_COIN_AMOUNT,
            balance_before=user_balance,
            balance_after=balance_after,
            type=CoinTransaction.TYPE_CLAIM
        )

from django.urls import path
from django.views.generic import TemplateView

app_name = 'common'

urlpatterns = [
    path(
        'api-docs/',
        TemplateView.as_view(
            template_name='common/api/docs.html',
            extra_context={'schema_url': 'swagger-ui'}
        ),
        name='docs'
    ),
    path(
        'swagger-ui/',
        TemplateView.as_view(template_name='common/api/docs.yml'),
        name='swagger-ui'
    )
]

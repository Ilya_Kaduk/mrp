openapi: 3.0.0

info:
  title: MRP API
  description: API for mrp project.
  version: 1.0.0

servers:
  - url: http://127.0.0.1:8000
    description: Local server

security:
  - BearerAuth: []

components:
  securitySchemes:
    BearerAuth:
      type: http
      scheme: bearer

  parameters:
    contentType:
      in: header
      name: Content-Type
      schema:
        type: string
        example: 'Content-Type: application/json'
      required: true
      description: For inform about type content.

    authorization:
      in: header
      name: Authorization
      schema:
        type: string
        example: 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
      required: true
      description: Access token for auth.

    ordering:
      in: query
      name: ordering
      schema:
        type: string
        example: '-created'
      required: false
      description: For order by desc or asc. Use the '-' symbol if desc ordering is desired.

    offset:
      in: query
      name: offset
      schema:
        type: integer
        example: 10
      required: false
      description: The number of items to skip before starting to collect the result set

    limit:
      in: query
      name: limit
      schema:
        type: integer
        example: 5
      required: false
      description: The numbers of items to return

  responses:
    noContent:
      description: No Content
    unauthorized:
      description: Unauthorized
    forbidden:
      description: Forbidden
    notFound:
      description: Not Found
    badRequest:
      description: Bad Request
      content:
        application/json:
          schema:
            type: object
            properties:
              field_name:
                type: array
                items:
                  type: string
                  example: Error message
    internalServerError:
      description: Internal Server Error

  requestBodies:
    TokenRefresh:
      description: JSON object containing refresh token.
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              refresh:
                type: string
                example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9

    CardCreate:
      description: JSON object containing card info.
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              type:
                type: string
                example: TYPE_CHEF

  schemas:
    AuthTokens:
      type: object
      properties:
        access:
          type: string
          example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
        refresh:
          type: string
          example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9

    CoinTransactionList:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
            example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
          amount:
            type: number
            format: float
            example: 1000.00000001
          balance_before:
            type: number
            format: float
            example: 0.00000000
          balance_after:
            type: number
            format: float
            example: 1000.00000001
          type:
            type: string
            example: TYPE_CLAIM
      example: [
        {
          'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
          'amount': 1000.00000001,
          'balance_before': 1000.00000001,
          'balance_after': 2000.00000002,
          'type': 'TYPE_CLAIM',
        },
        {
          'id': 'e1250766-d019-4fb9-8051-78cc652cdf93',
          'amount': 1000.00000001,
          'balance_before': 0.00000000,
          'balance_after': 1000.00000001,
          'type': 'TYPE_CLAIM',
        }
      ]

    CoinTransaction:
      type: object
      properties:
        id:
          type: string
          example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
        amount:
          type: number
          format: float
          example: 1000.00000001
        balance_before:
          type: number
          format: float
          example: 0.00000000
        balance_after:
          type: number
          format: float
          example: 1000.00000001
        type:
          type: string
          example: TYPE_CLAIM

    CardAttributeList:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
            example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
          name:
            type: string
            example: rarity
          value:
            type: string
            example: Raw
      example: [
        {
          'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
          'name': 'name',
          'value': 'Chef',
        },
        {
          'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
          'name': 'rarity',
          'value': 'Raw',
        }
      ]

    Card:
      type: object
      properties:
        id:
          type: string
          example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
        owner_id:
          type: string
          example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
        type:
          type: string
          example: TYPE_CHEF
        attributes:
          $ref: '#/components/schemas/CardAttributeList'

    CardList:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
            example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
          owner_id:
            type: string
            example: 1094aac3-bc6a-48c9-b6cd-dd59dfbba56a
          type:
            type: string
            example: TYPE_CHEF
          attributes:
            $ref: '#/components/schemas/CardAttributeList'
      example: [
        {
          'id': '1094aac3-bc6a-48c9-b6cd-dd59dfbba56a',
          'owner_id': '1094aac3-bc6a-48c9-b6cd-dd59dfbba56a',
          'type': 'TYPE_CHEF',
          'attributes': [
            {
              'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
              'name': 'name',
              'value': 'Chef'
            },
            {
              'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
              'name': 'rarity',
              'value': 'Raw'
            }
          ]
        },
        {
          'id': '1094aac3-bc6a-48c9-b6cd-dd59dfbba56a',
          'owner_id': '1094aac3-bc6a-48c9-b6cd-dd59dfbba56a',
          'type': 'TYPE_CHEF',
          'attributes': [
            {
              'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
              'name': 'name',
              'value': 'Chef'
            },
            {
              'id': 'e1250766-d019-4fb9-8051-78cc652cdf92',
              'name': 'rarity',
              'value': 'Raw'
            }
          ]
        }
      ]

paths:
  /api/v1/token/refresh/:
    post:
      tags:
        - 'Auth'
      description: Refreshes tokens.
      security: []
      requestBody:
        $ref: '#/components/requestBodies/TokenRefresh'
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuthTokens'
        '400':
          $ref: '#/components/responses/badRequest'
        '500':
          $ref: '#/components/responses/internalServerError'

  /api/v1/coins/claim/:
    post:
      tags:
        - "Coins"
      description: Claims coins.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
      responses:
        '200':
          description: OK
        '401':
          $ref: '#/components/responses/unauthorized'
        '500':
          $ref: '#/components/responses/internalServerError'

  /api/v1/coin-transactions/:
    get:
      tags:
        - "Coins"
      description: Returns user coin transactions.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
        - $ref: '#/components/parameters/ordering'
        - $ref: '#/components/parameters/offset'
        - $ref: '#/components/parameters/limit'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    example: 9
                  next:
                    example: null
                  previous:
                    example: null
                  results:
                    $ref: '#/components/schemas/CoinTransactionList'
        '401':
          $ref: '#/components/responses/unauthorized'
        '500':
          $ref: '#/components/responses/internalServerError'

  /api/v1/coin-transactions/id/:
    get:
      tags:
        - "Coins"
      description: Returns detail about coin transaction.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CoinTransaction'
        '401':
          $ref: '#/components/responses/unauthorized'
        '403':
          $ref: '#/components/responses/forbidden'
        '404':
          $ref: '#/components/responses/notFound'
        '500':
          $ref: '#/components/responses/internalServerError'

  /api/v1/cards/:
    post:
      tags:
        - "Cards"
      description: Creates card.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
      requestBody:
        $ref: '#/components/requestBodies/CardCreate'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Card'
        '400':
          $ref: '#/components/responses/badRequest'
        '401':
          $ref: '#/components/responses/unauthorized'
        '500':
          $ref: '#/components/responses/internalServerError'
    get:
      tags:
        - "Cards"
      description: Returns user cards.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
        - $ref: '#/components/parameters/ordering'
        - $ref: '#/components/parameters/offset'
        - $ref: '#/components/parameters/limit'
        - in: query
          name: type
          schema:
            type: string
            example: 'TYPE_CHEF'
          required: false
          description: To select cards by type
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  count:
                    example: 9
                  next:
                    example: null
                  previous:
                    example: null
                  results:
                    $ref: '#/components/schemas/CardList'
        '401':
          $ref: '#/components/responses/unauthorized'
        '500':
          $ref: '#/components/responses/internalServerError'

  /api/v1/cards/id/:
    get:
      tags:
        - "Cards"
      description: Returns detail about card.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Card'
        '401':
          $ref: '#/components/responses/unauthorized'
        '403':
          $ref: '#/components/responses/forbidden'
        '404':
          $ref: '#/components/responses/notFound'
        '500':
          $ref: '#/components/responses/internalServerError'
    delete:
      tags:
        - "Cards"
      description: Deletes card.
      parameters:
        - $ref: '#/components/parameters/contentType'
        - $ref: '#/components/parameters/authorization'
      responses:
        '204':
          $ref: '#/components/responses/noContent'
        '401':
          $ref: '#/components/responses/unauthorized'
        '403':
          $ref: '#/components/responses/forbidden'
        '404':
          $ref: '#/components/responses/notFound'
        '500':
          $ref: '#/components/responses/internalServerError'
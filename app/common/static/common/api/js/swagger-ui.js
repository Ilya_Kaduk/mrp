window.onload = () => {
  window.ui = SwaggerUIBundle({
    url: document.getElementById('swagger-url').getAttribute('data-swagger-url'),
    dom_id: '#swagger-ui',
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIBundle.SwaggerUIStandalonePreset
    ],
    layout: 'BaseLayout',
  });
};
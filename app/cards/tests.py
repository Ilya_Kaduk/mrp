from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from app.cards.models import Card, CardAttribute
from app.coins.models import CoinTransaction
from app.common.api.v1.utils import ApiTestUtil
from app.users.api.v1.utils import JWTAuthUtil
from app.users.models import User


class CardAPITest(APITestCase):
    util_class = ApiTestUtil
    auth_util_class = JWTAuthUtil
    request_client_class = APIClient

    def setUp(self):
        util = self.util_class()
        util.create_test_users()
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        util.claim_coins(user)
        util.claim_coins(user)
        card = Card.objects.create(owner=user, type=Card.TYPE_CHEF)
        attrs = card.prepare_bulk_create_attrs()
        CardAttribute.objects.bulk_create(attrs)
        balance_before = user.balance
        balance_after = balance_before - Card.TYPE_CHEF_PRICE
        CoinTransaction.objects.create(
            owner=user,
            amount=-Card.TYPE_CHEF_PRICE,
            balance_before=balance_before,
            balance_after=balance_after,
            type=CoinTransaction.TYPE_CREATE_CARD_CHEF
        )
        user.update_balance()

    def test_create(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.post(
            path=reverse('cards:card-list'),
            data={},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = request_client.post(
            path=reverse('cards:card-list'),
            data={'type': Card.TYPE_CHEF},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.json()
        self.assertEqual(
            list(response_data.keys()),
            ['id', 'owner_id', 'type', 'attributes']
        )
        attrs = response_data['attributes']
        self.assertIsInstance(attrs, list)
        self.assertEqual(list(attrs[0].keys()), ['id', 'name', 'value'])

    def test_detail(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.get(
            path=reverse('cards:card-detail', args=['card-id'])
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        card = user.cards.first()
        response = request_client.get(
            path=reverse('cards:card-detail', args=[card.id])
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_data = response.json()
        self.assertEqual(
            list(response_data.keys()),
            ['id', 'owner_id', 'type', 'attributes']
        )
        attrs = response_data['attributes']
        self.assertIsInstance(attrs, list)
        self.assertEqual(list(attrs[0].keys()), ['id', 'name', 'value'])

        user = User.objects.get(username=ApiTestUtil.USERS[1].username)
        tokens = auth_util.refresh_tokens(user)
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.get(
            path=reverse('cards:card-detail', args=[card.id])
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_list(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        path = '{}{}'.format(reverse('cards:card-list'), '?type=TYPE_CHEF')
        response = request_client.get(
            path=path,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_data = response.json()
        self.assertIn('results', response_data)

        results = response_data['results']
        self.assertIsInstance(results, list)
        self.assertTrue(len(results) == 1)

        card = results[0]
        self.assertEqual(
            list(card.keys()),
            ['id', 'owner_id', 'type', 'attributes']
        )
        self.assertEqual(card['type'], Card.TYPE_CHEF)

        attrs = card['attributes']
        self.assertIsInstance(attrs, list)
        self.assertEqual(list(attrs[0].keys()), ['id', 'name', 'value'])

        path = '{}{}'.format(reverse('cards:card-list'), '?type=INVALID_TYPE')
        response = request_client.get(
            path=path,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        cards = response.json()['results']
        self.assertEqual(cards, [])

    def test_delete(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        card = user.cards.first()
        response = request_client.delete(
            path=reverse('cards:card-detail', args=[card.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        user = User.objects.get(username=ApiTestUtil.USERS[1].username)
        tokens = auth_util.refresh_tokens(user)
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.get(
            path=reverse('cards:card-detail', args=[card.id])
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

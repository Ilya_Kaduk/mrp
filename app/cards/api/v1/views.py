from django.db import transaction
from rest_framework import viewsets, status
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from app.cards.api.v1.filters import CardFilter
from app.cards.api.v1.serializers import CardSerializer
from app.cards.models import Card, CardAttribute
from app.coins.models import CoinTransaction
from app.common.api.v1.filters import OwnerFilter
from app.users.models import User


class CardView(viewsets.ModelViewSet):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    filter_backends = [OwnerFilter, OrderingFilter, CardFilter]
    ordering_fields = ['created']
    http_method_names = ['get', 'post', 'delete']
    response_class = Response

    def create(self, request, *args, **kwargs):
        user_qs = User.objects.select_for_update().filter(id=request.user.id)
        with transaction.atomic():
            user = user_qs.get()
            user.update_balance()

            serializer = self.get_serializer(
                data=request.data,
                context={'user': user}
            )
            serializer.is_valid(raise_exception=True)
            card = serializer.save(owner=request.user)

            attrs = card.prepare_bulk_create_attrs()
            CardAttribute.objects.bulk_create(attrs)

            balance_before = user.balance
            balance_after = balance_before - Card.TYPE_CHEF_PRICE
            CoinTransaction.objects.create(
                owner=user,
                amount=-Card.TYPE_CHEF_PRICE,
                balance_before=balance_before,
                balance_after=balance_after,
                type=CoinTransaction.TYPE_CREATE_CARD_CHEF
            )

            user.update_balance()
            return self.response_class(
                data=serializer.data,
                status=status.HTTP_201_CREATED
            )

from django.utils.translation import gettext as _

from rest_framework import serializers

from app.cards.models import Card, CardAttribute


class CardAttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardAttribute
        fields = ['id', 'name', 'value']


class CardSerializer(serializers.ModelSerializer):
    attributes = CardAttributeSerializer(many=True, read_only=True)

    class Meta:
        model = Card
        fields = ['id', 'owner_id', 'type', 'attributes']

    def validate(self, attrs):
        user = self.context['user']
        if user.balance < Card.TYPES_PRICES[attrs['type']]:
            raise serializers.ValidationError(_('Not enough funds'))
        return attrs

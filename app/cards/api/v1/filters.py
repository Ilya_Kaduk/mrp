from rest_framework import filters


class CardFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        card_type = request.query_params.get('type')
        if card_type:
            queryset = queryset.filter(type=card_type)
        return queryset

from rest_framework.routers import SimpleRouter

from app.cards.api.v1 import views

app_name = 'cards'

urlpatterns = []

router = SimpleRouter()
router.register('cards', views.CardView)
urlpatterns += router.urls

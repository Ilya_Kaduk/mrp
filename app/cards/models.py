import uuid

from django.conf import settings
from django.db import models

from app.users.models import User


class Card(models.Model):
    TYPE_CHEF = 'TYPE_CHEF'

    TYPE_CHOICES = [
        (TYPE_CHEF, TYPE_CHEF),
    ]

    TYPE_CHEF_PRICE = settings.CARD_TYPE_CHEF_PRICE

    TYPES_PRICES = {
        TYPE_CHEF: TYPE_CHEF_PRICE
    }

    ATTRIBUTES = {
        TYPE_CHEF: [
            {
                'name': 'name',
                'value': 'Chef'
            },
            {
                'name': 'rarity',
                'value': 'Raw'
            }
        ]
    }

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='cards'
    )
    type = models.CharField(max_length=255, choices=TYPE_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def prepare_bulk_create_attrs(self):
        attrs = []
        for attr in self.ATTRIBUTES[self.type]:
            attrs.append(
                CardAttribute(
                    card=self,
                    name=attr['name'],
                    value=attr['value']
                )
            )
        return attrs


class CardAttribute(models.Model):
    class Meta:
        db_table = 'cards_card_attribute'

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    card = models.ForeignKey(
        Card,
        on_delete=models.CASCADE,
        related_name='attributes'
    )
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

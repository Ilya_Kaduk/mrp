from rest_framework import status
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient

from app.common.api.v1.utils import ApiTestUtil
from app.users.api.v1.utils import JWTAuthUtil
from app.users.models import User


class CoinClaimAPITest(APITestCase):
    util_class = ApiTestUtil
    auth_util_class = JWTAuthUtil
    request_client_class = APIClient

    def setUp(self):
        util = self.util_class()
        util.create_test_users()

    def test_coins_claim_success(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.post(
            path=reverse('coins:coins-claim'),
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CoinTransactionAPITest(APITestCase):
    util_class = ApiTestUtil
    auth_util_class = JWTAuthUtil
    request_client_class = APIClient

    def setUp(self):
        util = self.util_class()
        util.create_test_users()
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        util.claim_coins(user)

    def test_list(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        response = request_client.get(
            path=reverse('coins:cointransaction-list')
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        coin_transactions = response.json()
        self.assertIn('results', coin_transactions)
        results = coin_transactions['results']
        self.assertIsInstance(results, list)
        self.assertTrue(len(results) > 0)
        self.assertEqual(
            list(results[0].keys()),
            ['id', 'amount', 'balance_before', 'balance_after', 'type']
        )

    def test_detail(self):
        user = User.objects.get(username=ApiTestUtil.USERS[0].username)
        auth_util = self.auth_util_class()
        tokens = auth_util.refresh_tokens(user)
        request_client = self.request_client_class()
        request_client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(tokens['access'])
        )
        coin_transaction = user.coin_transactions.first()
        response = request_client.get(
            path=reverse(
                'coins:cointransaction-detail',
                args=[coin_transaction.id]
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = response.json()
        self.assertEqual(
            list(response_data.keys()),
            ['id', 'amount', 'balance_before', 'balance_after', 'type']
        )

import uuid

from django.db import models

from app.users.models import User


class CoinTransaction(models.Model):
    class Meta:
        db_table = 'coins_coin_transaction'

    TYPE_DEPOSIT = 'TYPE_DEPOSIT'
    TYPE_WITHDRAWAL = 'TYPE_WITHDRAWAL'
    TYPE_CLAIM = 'TYPE_CLAIM'
    TYPE_CREATE_CARD_CHEF = 'TYPE_CREATE_CARD_CHEF'

    TYPE_CHOICES = [
        (TYPE_DEPOSIT, TYPE_DEPOSIT),
        (TYPE_WITHDRAWAL, TYPE_WITHDRAWAL),
        (TYPE_CLAIM, TYPE_CLAIM),
        (TYPE_CREATE_CARD_CHEF, TYPE_CREATE_CARD_CHEF),
    ]

    TYPE_CLAIM_COIN_AMOUNT = 1000

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='coin_transactions'
    )
    amount = models.DecimalField(max_digits=16, decimal_places=8)
    balance_before = models.DecimalField(max_digits=16, decimal_places=8)
    balance_after = models.DecimalField(max_digits=16, decimal_places=8)
    type = models.CharField(max_length=255, choices=TYPE_CHOICES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

from rest_framework import serializers

from app.coins.models import CoinTransaction


class CoinTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoinTransaction
        fields = [
            'id',
            'amount',
            'balance_before',
            'balance_after',
            'type'
        ]

from django.urls import path
from rest_framework.routers import SimpleRouter

from app.coins.api.v1 import views

app_name = 'coins'

urlpatterns = [
    path('coins/claim/', views.CoinClaimView.as_view(), name='coins-claim')
]

router = SimpleRouter()
router.register('coin-transactions', views.CoinTransactionView)
urlpatterns += router.urls

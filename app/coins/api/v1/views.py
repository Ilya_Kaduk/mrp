from django.db import transaction
from rest_framework import status, viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
from rest_framework.views import APIView

from app.coins.api.v1.serializers import CoinTransactionSerializer
from app.coins.models import CoinTransaction
from app.common.api.v1.filters import OwnerFilter
from app.users.models import User


class CoinClaimView(APIView):
    response_class = Response

    def post(self, request):
        user_qs = User.objects.select_for_update().filter(id=request.user.id)
        with transaction.atomic():
            user = user_qs.get()
            user.update_balance()
            balance_before = user.balance
            balance_after = \
                balance_before + CoinTransaction.TYPE_CLAIM_COIN_AMOUNT
            CoinTransaction.objects.create(
                owner=user,
                amount=CoinTransaction.TYPE_CLAIM_COIN_AMOUNT,
                balance_before=balance_before,
                balance_after=balance_after,
                type=CoinTransaction.TYPE_CLAIM
            )
            user.update_balance()
            return self.response_class(status=status.HTTP_200_OK)


class CoinTransactionView(viewsets.ReadOnlyModelViewSet):
    queryset = CoinTransaction.objects.all()
    serializer_class = CoinTransactionSerializer
    filter_backends = [OwnerFilter, OrderingFilter]
    ordering_fields = ['created']

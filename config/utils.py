import os


def get_config_variable(name):
    variable = os.environ[name] if name in os.environ else os.getenv(name)
    if variable:
        return variable
    raise ValueError(f'Error: variable <{name}> missed. Check env variables.')

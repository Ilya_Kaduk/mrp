from django.http import JsonResponse
from django.urls import path, include

api_v1_prefix = 'api/v1/'

urlpatterns = [
    path('health-check/', lambda request: JsonResponse({}, status=200)),
    path('', include('app.common.api.urls')),
    path(api_v1_prefix, include('app.coins.api.v1.urls')),
    path(api_v1_prefix, include('app.cards.api.v1.urls')),
    path(api_v1_prefix, include('app.users.api.v1.urls')),
]

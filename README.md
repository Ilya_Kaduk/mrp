### MPR

MPR is an API for work with cards and coin transactions. 
A user can create a card to participate in coin mining. 
Creating a card costs a certain amount of coins. 

### System requirements

Linux, macOS or Windows.

### Tools requirements

Git

Docker 24.0.7

Docker Compose 1.29.1

Make

GNU make (only for Windows)

Note: see the guide on how to install this tools on your system on the official websites.

### Deployment

Note: if you have Nginx or PostgreSQL installed in your system, you need to stop them.

For Ubuntu:
```bash
sudo systemctl status postgresql
sudo systemctl stop postgresql

sudo systemctl status nginx
sudo systemctl stop nginx
```

Create the images and run the containers.

```bash
make build

make up
```

Make sure the containers are running.

```bash
make check-containers
```

If for some reason some containers are not started, run the make up command again.

Check that locales have been successfully added to the database container and 
the required database has been created (database name is mrp). 

```bash
make check-locales

make check-db-exists
```

Run the database migrations.

```bash
make migrate
```

Check if migrations have been applied.

```bash
make check-db-tables
```

Collect static files.

```bash
make collect-static
```

Create a user to test the API. You will receive an access and refresh token in response. 
Save them to use them in future API requests.

```bash
make create-user username=JohnDoe first_name=John last_name=Doe email=johndoe@mrp.com password=asdf1234
```

Health check your app.

For local deploy:
```bash
curl http://127.0.0.1:1337/health-check/
```

Run tests.

```bash
make test
```

### Check API docs

Go to http://127.0.0.1:1337/api-docs/

### How it works?

The user claims coins for the test.

All transactions related to coins are written to the database. You can see the list of transactions and a specific transaction.

Coins are needed to create a chef card. 

The user can create, delete and view one or all of his cards.
